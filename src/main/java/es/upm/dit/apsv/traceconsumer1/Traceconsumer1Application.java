package es.upm.dit.apsv.traceconsumer1;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import es.upm.dit.apsv.traceconsumer1.model.Trace;
import es.upm.dit.apsv.traceconsumer1.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer1.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer1.repository.TransportationOrderRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SpringBootApplication
public class Traceconsumer1Application {

		public static final Logger log = LoggerFactory.getLogger(Traceconsumer1Application.class);

        @Autowired
        private  TraceRepository traceRepository;

        @Autowired
        private  Environment env;

		@Autowired
		private  TransportationOrderRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer1Application.class, args);
	}

		@Bean("consumer")
        public Consumer<Trace> checkTrace() {
                return t -> {
                        t.setTraceId(t.getTruck() + t.getLastSeen());
                        traceRepository.save(t);
						Optional<TransportationOrder> ot = repository.findById(t.getTruck());
						if(ot.isPresent()){
							TransportationOrder result = ot.get();
							result.setLastDate(t.getLastSeen());
							result.setLastLat(t.getLat());
							result.setLastLong(t.getLng());
							if (result.distanceToDestination() < 10)
								result.setSt(1);
							repository.save(result);
						}
						//TraceConsumer 1
						/*String uri = env.getProperty("orders.server");
						RestTemplate restTemplate = new RestTemplate();
						TransportationOrder result = null;
						try {
							result = restTemplate.getForObject(uri + t.getTruck(), TransportationOrder.class);
						}catch (HttpClientErrorException.NotFound ex){
							result = null;
						}
						if (result != null && result.getSt() == 0) {
							result.setLastDate(t.getLastSeen());
							result.setLastLat(t.getLat());
							result.setLastLong(t.getLng());
							if (result.distanceToDestination() < 10)
								result.setSt(1);
							restTemplate.put(uri,result,TransportationOrder.class);
							log.info("Order updated: "+ result);
						}*/
             	};
			}

}
